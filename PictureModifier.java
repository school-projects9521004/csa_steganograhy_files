import java.awt.*;

public class PictureModifier {
    public static final int bits = 2;
    public static final int inverse = 8 - bits;
    public static final int mask = ~ (0xff >> inverse);
    public static int hideInt(int c1, int c2) {
        return (c1 & mask) | (c2 >> inverse);
    }
    public static int getHiddenInt(int source) {
        return source << inverse & 0xff;
    }
    public static Color hideColor(Color source, Color secret) {
        if (bits > 7) return secret;
        return new Color(
            hideInt(source.getRed(), secret.getRed()),
            hideInt(source.getGreen(), secret.getGreen()),
            hideInt(source.getBlue(), secret.getBlue())
        );
    }
    public static Color getHiddenColor(Color source) {
        return new Color(
            getHiddenInt(source.getRed()),
            getHiddenInt(source.getGreen()),
            getHiddenInt(source.getBlue())
        );
    }
    public static boolean cannotHidePicture(Picture source, Picture secret) {
        return source.getWidth() < secret.getWidth() || source.getHeight() < secret.getHeight();
    }
    public static Picture hidePicture(Picture source, Picture secret) {
        if (cannotHidePicture(source, secret)) {
            System.out.println("Cannot hide picture due to size difference.");
            return source;
        }

        Picture pic = new Picture(source);
        Pixel[][] source_pixels = source.getPixels2D();
        Pixel[][] secret_pixels = secret.getPixels2D();
        int w_start = (int) (Math.random() * (source_pixels.length - secret_pixels.length));
        int h_start = (int) (Math.random() * (source_pixels[0].length - secret_pixels[0].length));
        int w_end = secret_pixels.length + w_start;
        int h_end = secret_pixels[0].length + h_start;

        for (int w = w_start; w < w_end; w++) {
            for (int h = h_start; h < h_end; h++) {
                Color source_color = source_pixels[w][h].getColor();
                Color secret_color = secret_pixels[w - w_start][h - h_start].getColor();
                pic.setBasicPixel(h, w, hideColor(source_color, secret_color).getRGB());
            }
        }
        return pic;

    }
    public static Picture getHiddenPicture(Picture source) {
        Picture n = new Picture(source.getHeight(), source.getWidth());
        Pixel[][] source_pixels = source.getPixels2D();

        for (int w = 0; w < source_pixels.length; w++) {
            for (int h = 0; h < source_pixels[0].length; h++) {
                Color source_color = source_pixels[w][h].getColor();
                n.setBasicPixel(h, w, getHiddenColor(source_color).getRGB());
            }
        }
        return n;
    }
    public static void main(String[] args)
    {
        Picture pic1 = new Picture("beach.jpg");
        Picture pic2 = new Picture("flower1.jpg");
        Picture out = hidePicture(pic1, pic2);
        out.show();
        getHiddenPicture(out).show();

    }
}


